import alias from '@rollup/plugin-alias';
import babel from '@rollup/plugin-babel';
import builtins from 'builtin-modules';
import commonjs from '@rollup/plugin-commonjs';
import del from 'rollup-plugin-delete';
import { dependencies } from './package.json';
import graphql from '@kocal/rollup-plugin-graphql';
import path from 'path';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

const OUTPUT_DIR = 'dist';
const ASSET_FILE_TYPES = ['.graphql'];
const SCRIPT_FILE_TYPES = ['.ts'];
const FILE_TYPES = [...ASSET_FILE_TYPES, ...SCRIPT_FILE_TYPES];

export default {
	input: 'src/index.ts',
	output: {
		dir: OUTPUT_DIR,
		format: 'cjs',
		sourcemap: true
	},
	plugins: [
		del({ targets: OUTPUT_DIR }),
		alias({
			resolve: FILE_TYPES,
			entries: [
				{
					find: /^~\/(?<path>.*)/u,
					replacement: path.resolve(__dirname, 'src', '$1')
				}
			]
		}),
		resolve({ extensions: FILE_TYPES, preferBuiltins: true }),
		commonjs(),
		graphql(),
		babel({
			babelHelpers: 'bundled',
			extensions: SCRIPT_FILE_TYPES,
			exclude: 'node_modules/**'
		}),
		terser()
	],
	external: [
		...builtins,
		...Object.keys(dependencies || {}),
		'lodash.includes',
		'lodash.isnumber',
		'lodash.isinteger',
		'lodash.isplainobject',
		'lodash.once',
		'lodash.isboolean',
		'lodash.isstring',
		'ms'
	]
};
