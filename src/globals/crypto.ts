export { APP_SECRET } from '~/config';

export const SALT_ROUNDS = 10;
