import { GraphQLServer } from 'graphql-yoga';
import log from '~/modules/log';
import resolvers from '~/resolvers';
import schema from '~/schema';
import {
	closeConnection,
	db
} from '~/modules/db';

const main = (): void => {
	const server = new GraphQLServer({
		typeDefs: schema,
		resolvers,
		context: request => ({
			...request,
			db
		})
	});
	server
		.start(() => log.success('Server is running on port 4000'))
		.catch(err => log.error(err));
};

try {
	main();
} catch (err) {
	log.error(err);
} finally {
	closeConnection().catch(err => log.error(err));
}
