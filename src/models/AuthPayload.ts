import { User } from '~/models/User';

export type AuthPayload = {
	token: string;
	user: User;
};
