/* eslint-disable camelcase,@typescript-eslint/naming-convention */
import { User } from '~/models/User';

export type Pokemon = {
	id: number;
	species: string;
	pokemon: string;
	nickname: string;
	version: string;
	box: number;
	slot: number;
	owner_id: number;
	owner: User;
	date_created: Date;
	date_updated: Date;
};
