export type QueryParamsPagination = {
	skip: number;
	take: number;
};
