import { LoginArgs } from '~/models/LoginArgs';

export type RegistrationArgs = LoginArgs & {
	displayName: string;
};
