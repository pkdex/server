/* eslint-disable camelcase,@typescript-eslint/naming-convention */
import { Pokemon } from '~/models/Pokemon';

export type User = {
	id: number;
	email: string;
	password: string;
	display_name: string;
	pokemon?: Pokemon[];
	date_created: Date|string;
	date_updated: Date|string;
};
