import consola from 'consola';

export default {
	log: (msg: string): void => consola.log(msg),
	error: (err: Readonly<Error>, msg?: string): void => consola.error(err, msg),
	info: (msg: string): void => consola.info(msg),
	warn: (msg: string): void => consola.warn(msg),
	success: (msg: string): void => consola.success(msg)
};
