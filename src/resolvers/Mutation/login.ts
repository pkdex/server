import { APP_SECRET } from '~/globals/crypto';
import { AuthPayload } from '~/models/AuthPayload';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { LoginArgs } from '~/models/LoginArgs';

export const login = async (parent: unknown, {
	email,
	password
}: Readonly<LoginArgs>, { db }): Promise<AuthPayload> => {
	const user = await db.user.findOne({ where: { email } });
	if (!user) throw new Error('No such user found');

	const passwordIsValid = await bcrypt.compare(password, user.password);
	if (!passwordIsValid) throw new Error('Invalid password');

	const token = jwt.sign({ userId: user.id }, APP_SECRET);

	return { token, user };
};
