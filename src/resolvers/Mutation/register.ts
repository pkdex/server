import { AuthPayload } from '~/models/AuthPayload';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { RegistrationArgs } from '~/models/RegistrationArgs';
import {
	APP_SECRET,
	SALT_ROUNDS
} from '~/globals/crypto';

export const register = async (parent: unknown, {
	email,
	password,
	displayName
}: Readonly<RegistrationArgs>, { db }): Promise<AuthPayload> => {
	const encryptedPassword = await bcrypt.hash(password, SALT_ROUNDS);
	const user = await db.user.create({
		data: {
			email,
			password: encryptedPassword,

			// eslint-disable-next-line camelcase,@typescript-eslint/naming-convention
			display_name: displayName
		}
	});
	const token = jwt.sign({ userId: user.id }, APP_SECRET);

	return { token, user };
};
