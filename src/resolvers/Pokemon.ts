import { Pokemon } from '~/models/Pokemon';
import { User } from '~/models/User';

// eslint-disable-next-line camelcase,@typescript-eslint/naming-convention
export const owner = async ({ owner_id }: Readonly<Pokemon>, { db }): Promise<User> => await db.user.findOne({ where: { id: owner_id } });
