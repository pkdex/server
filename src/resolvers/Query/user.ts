import { QueryParamsId } from '~/models/QueryParamsId';
import { User } from '~/models/User';

export const user = async (parent: unknown, { id }: Readonly<QueryParamsId>, { db }): Promise<User|null> => await db.user.findOne({ where: { id } });
