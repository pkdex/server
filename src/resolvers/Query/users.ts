import { QueryParamsPagination } from '~/models/QueryParamsPagination';
import { User } from '~/models/User';

export const users = async (parent: unknown, { skip, take }: Readonly<QueryParamsPagination>, { db }): Promise<User[]> => await db.user.findMany({ skip, take });
