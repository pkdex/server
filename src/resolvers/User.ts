import { Pokemon } from '~/models/Pokemon';
import { User } from '~/models/User';

// eslint-disable-next-line camelcase,@typescript-eslint/naming-convention
export const pokemon = async ({ id }: Readonly<User>, { db }): Promise<Pokemon[]> => await db.pokemon.findMany({ where: { owner_id: id } });
