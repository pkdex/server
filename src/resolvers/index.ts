/* eslint-disable camelcase,@typescript-eslint/naming-convention */
import * as Mutation from '~/resolvers/Mutation';
import * as Pokemon from '~/resolvers/Pokemon';
import * as Query from '~/resolvers/Query';
import * as User from '~/resolvers/User';
import {
	GraphQLScalarType,
	Kind,
	ValueNode
} from 'graphql';

const resolvers = {
	Date: new GraphQLScalarType({
		name: 'Date',
		description: 'Date custom scalar type',
		parseValue: (value: number): Date => new Date(value),
		serialize: (value: Readonly<Date>): number => value.getTime(),
		parseLiteral: (valueAST: ValueNode): number|null => {
			if (valueAST.kind === Kind.INT) return parseInt(valueAST.value);

			return null;
		}
	}),
	Mutation,
	Pokemon,
	Query,
	User
};

export default resolvers;
