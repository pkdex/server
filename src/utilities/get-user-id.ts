import { APP_SECRET } from '~/globals/crypto';
import jwt from 'jsonwebtoken';

const getUserId = ({ request }): number => {
	const Authorization = request.get('Authorization');
	if (!Authorization) throw new Error('Not authenticated');
	const token = Authorization.replace('Bearer ', '');
	const { userId } = jwt.verify(token, APP_SECRET);

	return userId;
};
